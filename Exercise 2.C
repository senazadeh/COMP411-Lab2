#include <stdio.h>
#include <math.h>

int main() 
{
  double input;
  double sum = 0;
  double min = INFINITY;
  double max = -INFINITY;
  double product = 1;
  printf("Enter 10 floating-point numbers:\n");
  for (int i  = 0; i < 10; i++) {
    scanf("%lf", &input);
    sum += input;
    product *= input;
    if (input < min) { min = input; }
    if (input > max) { max = input; }
  }  
  printf("Sum is %0.5lf\n", sum);
  printf("Min is %0.5lf\n", min);
  printf("Max is %0.5lf\n", max);
  printf("Product is %0.5lf\n", product);
}
